
### Hi there 👋

My Name is @julioRomero.
I'm a Police Officer and a SSR Full Stack Developer. I am a lover ❣️ of technology and application development. I like ⚽ and 🎸. I like to travel and see many places.
Every day I try to be a better developer, learning technology, skills and good practices. I like working with people and creating products as a team.

## Currently working on 

  Policia del Chubut as a 🖥️ Computer and 🎙️ Communication Technician. I also work daily on small personal projects and as a collaborator with the aim of improving my skills.

## 📚 Learning 

  NestJs, ReactJs, VueJs, Python, Django, among others.
  I try to learn new tools, libraries, frameworks every day, which allow me to be more agile and improve the quality of my projects

## 💻 Studies completed

  ***Title***: *ElectroMechanical Technician*
  
  ***Year***: *1999*
  
  ***School***: *Politecnica 701*
  
  <br>
  
  ***Title***: *Analista Programador Universitario*
  
  ***Year***: *2017*
  
  ***University***: *UNPAT San Juan Bosco*



